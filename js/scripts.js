$( document ).ready(function() {
    $('#navigation a').click(function() {
        $('#navigation li').removeClass('active');
        $(this).parents('li').addClass('active');

        if('users' == $(this).data('target')) {
            $('.groups-holder').css('display','none');
            $('.users-holder').fadeIn();
        }else {
            $('.users-holder').css('display','none');
            $('.groups-holder').fadeIn();
        }
    });

    $('.delete-user').click(function (e) {
        e.preventDefault();
        var target = $(this).parents('tr');

        if(!confirm('Do you really want to Delete?')){
            return;
        }

        $.ajax({
            type: "POST",
            url: 'deleteUser.php',
            data: {'id': $(this).data('id')},
            success: function(data) {
                if('true' == data) {
                    target.fadeOut()
                }
            },
        });
    });
    $('.delete-group').click(function (e) {
        e.preventDefault();
        var target = $(this).parents('tr');
        var target2 = $('.group-' + $(this).data('id'));

        console.log(target2);
        if(!confirm('Do you really want to Delete?')){
            return;
        }

        $.ajax({
            type: "POST",
            url: 'deleteGroup.php',
            data: {'id': $(this).data('id')},
            success: function(data) {
                if('true' == data) {
                    target.fadeOut()
                    target2.fadeOut()
                }
            },
        });
    });
});