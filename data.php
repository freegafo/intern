<?php
global $conn;
$servername = "127.0.0.1";
$username = "root";
$password = "";
$database = "test_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


class Users {

    public function all_users() {
        global $conn;
        $result = mysqli_query($conn,"SELECT * FROM users");
        if($result)
            return($result->fetch_all());
        else
            return;
    }

    public function user($id) {
        global $conn;
        $result = mysqli_query($conn,"SELECT * FROM users WHERE id = $id");
        $groups = mysqli_query($conn,"SELECT * FROM user_groups WHERE user_id = $id");

        $user['user'] = $result->fetch_row();
        $user['groups'] = $groups->fetch_all();

        if($result)
            return $user;
        else
            return;
    }

    public function all_groups() {
        global $conn;
        $result = mysqli_query($conn,"SELECT * FROM groups");
        if($result)
            return($result->fetch_all());
        else
            return;
    }

    public function user_groups($id) {
        global $conn;
        $result = mysqli_query($conn,"SELECT * FROM user_groups WHERE user_id = $id");
        if($result)
            return($result->fetch_all());
        else
            return;
    }
    public function group_name($id) {
        global $conn;
        $result = mysqli_query($conn,"SELECT * FROM groups WHERE id = $id");
        $result = $result->fetch_row();

        if($result)
            return $result[1];
        else
            return;
    }


    public function user_update($user = array()) {
        global $conn;

        $sql = "UPDATE users ".
            "SET username = '$user[user]' ,
             name = '$user[name]',
             email = '$user[email]',
             password = '$user[password]',
             role = '$user[role]' ".
            "WHERE id =  $user[id]";

        $retval = mysqli_query($conn, $sql);
        if(!$retval )
        {
            return 'Could not update data: ' . mysql_error();
        }


        $sql = "DELETE FROM user_groups WHERE user_id =$user[id]";
        $retval = mysqli_query($conn, $sql);

        if(!$retval )
            return 'Could not update data groups del: ' . mysql_error();

        if($user['groups']) {
            foreach ($user['groups'] as $g){
                $sql = "INSERT INTO user_groups (user_id, group_id) VALUES ('$user[id]', '$g')";
                $retval = mysqli_query($conn, $sql);

                if(!$retval )
                    return 'Could not update data groups: ' . mysql_error();
            }
        }

        return "Updated data successfully <br> <a href='index.php'>Go Back</a>";
    }

    public function user_add($user = array()) {
        global $conn;

        $sql = "INSERT INTO users (username, name, email, password, role)
            VALUES ('$user[user]', '$user[name]', '$user[email]', '$user[password]', '$user[role]')
       ";

        $retval = mysqli_query($conn, $sql);
        $user_id = mysqli_insert_id($conn);

        if(!$retval )
            return 'Could not update data: ' . mysql_error();

        if($user['groups']) {
            foreach ($user['groups'] as $g){
                $sql = "INSERT INTO user_groups (user_id, group_id) VALUES ('$user_id', '$g')";
                $retval = mysqli_query($conn, $sql);

                if(!$retval )
                    return 'Could not update data: ' . mysql_error();
            }
        }

        return "Updated data successfully <br> <a href='index.php'>Go Back</a>";
    }

    public function user_delete($id) {
        global $conn;

        $sql = "DELETE FROM users WHERE id = $id";
        $sql2 = "DELETE FROM user_groups WHERE user_id =$id";

        $retval = mysqli_query($conn, $sql);
        $retval2 = mysqli_query($conn, $sql2);

        if($retval ) {
            return 'true';
        }

        return 'false';
    }

    public function group_delete($id) {
        global $conn;

        $sql = "DELETE FROM groups WHERE id = $id";
        $sql2 = "DELETE FROM user_groups WHERE group_id =$id";

        $retval = mysqli_query($conn, $sql);
        $retval2 = mysqli_query($conn, $sql2);

        if($retval ) {
            return 'true';
        }

        return 'false';
    }

    public function group($id) {
        global $conn;
        $result = mysqli_query($conn,"SELECT * FROM groups WHERE id = $id");

        if($result)
            return $result->fetch_row();
        else
            return;
    }

    public function group_add($group) {
        global $conn;

        $sql = "INSERT INTO groups (name)
            VALUES ('$group[name]')
       ";

        var_dump($sql);
        $retval = mysqli_query($conn, $sql);

        if(!$retval )
            return 'Could not update data: ' . mysql_error();

        return "Updated data successfully <br> <a href='index.php'>Go Back</a>";
    }

    public function group_update($group) {
        global $conn;

        $sql = "UPDATE groups ".
            "SET name = '$group[name]' WHERE id =  $group[id]";

        $retval = mysqli_query($conn, $sql);

        if(!$retval )
            return 'Could not update data: ' . mysql_error();

        return "Updated data successfully <br> <a href='index.php'>Go Back</a>";
    }
}