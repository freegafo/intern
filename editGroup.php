<?php require('data.php'); ?>
<?php require('header.php'); ?>

<?php
$group_id = $_GET["id"];
$users = new Users();
$group = $users->group($group_id);

?>
    <div class="row">
        <div class="large-12 columns">
            <h1>Edit User</h1>
            <form action="saveGroup.php" method="post">
                <label>Name
                    <input type="text" name="name" value="<?php echo $group[1] ?>" />
                </label>
                <input type="hidden" name="id" value="<?php echo $group[0] ?>" />
                <input type="hidden" name="user_action" value="edit" />
                <p>
                    <input type="submit" class="button right" />
                    <a href="index.php" class="button alert right">Cancel</a>
                </p>
            </form>
        </div>
    </div>

<?php require('footer.php'); ?>