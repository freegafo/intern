<?php require('data.php'); ?>
<?php require('header.php'); ?>

<?php
    $users = new Users();

?>
<nav id="navigation">
    <ul>
        <li class="active"><a href="#" data-target="users"  >Users</a></li>
        <li><a href="#" data-target="groups">Groups</a></li>
    </ul>
</nav>


<div class="row">
    <div class="large-12 columns">
        <div class="users-holder">
            <h1>All Users</h1>
            <a href="addUser.php">add User</a>
            <table class="users">
                <thead>
                    <tr>
                        <th width="5%">N</th>
                        <th width="10%">User</th>
                        <th width="15%">Name</th>
                        <th width="15%">Email</th>
                        <th width="35%">Groups</th>
                        <th width="10%">Role</th>
                        <th width="5%">Edit</th>
                        <th width="5%">Delete</th>
                    </tr>
                </thead>
                    <?php
                    $count = 0;
                    //                    var_dump($users->all_users());
                    foreach ($users->all_users() as $user):

                        $count++;
                        ?>
                        <tr>
                            <td><?php echo $count; ?></td>
                            <td><?php echo $user[1]; ?></td>
                            <td><?php echo $user[2]; ?></td>
                            <td><?php echo $user[3]; ?></td>
                            <td>
                                <?php
                                     foreach ($users->user_groups($user[0]) as $g){
                                         echo "<span class='group-". $g[2] ."'>" . $users->group_name($g[2]).' / </span>';
                                     }
                                 ?>
                            </td>
                            <td><?php echo $user[5]; ?></td>
                            <td>
                                <a href="editUser.php?id=<?php echo $user[0]; ?>">
                                    <img src="images/edit.png" alt="" width="32" height="32" />
                                </a>
                            </td>
                            <td>
                                <a href="#" class="delete-user" data-id="<?php echo $user[0]; ?>">
                                    <img src="images/delete.png" alt="" width="32" height="32" />
                                </a>
                            </td>
                    </tr>
                    <?php endforeach; ?>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="groups-holder">
            <h1>All Groups</h1>
            <a href="addGroup.php">add Group</a>
            <table class="groups" width="100%">
                <thead>
                <tr>
                    <th width="30">N</th>
                    <th>Group</th>
                    <th width="50">Edit</th>
                    <th width="50">Delete</th>
                </tr>
                </thead>
                <?php
                $count = 0;
                //                    var_dump($users->all_users());
                foreach ($users->all_groups() as $g):

                    $count++;
                    ?>
                    <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $g[1]; ?></td>
                        <td>
                            <a href="editGroup.php?id=<?php echo $g[0]; ?>">
                                <img src="images/edit.png" alt="" width="32" height="32" />
                            </a>
                        </td>
                        <td>
                            <a href="#" class="delete-group" data-id="<?php echo $g[0]; ?>">
                                <img src="images/delete.png" alt="" width="32" height="32" />
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php require('footer.php'); ?>