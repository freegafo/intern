<?php require('data.php'); ?>
<?php require('header.php'); ?>

<?php
$user_id = $_GET["id"];
$users = new Users();
$data = $users->user($user_id);
$user = $data['user'];

$groups = array();

foreach($data['groups'] as $g) {
    $groups[] = $g[2];
}

?>
    <div class="row">
        <div class="large-12 columns">
            <h1>Edit User</h1>
            <form action="saveUser.php" method="post">
                <label>User
                        <input type="text" name="username" value="<?php echo $user[1] ?>" />
                </label>
                <label>Name
                    <input type="text" name="name"  value="<?php echo $user[2] ?>" />
                </label>
                <label>Email
                    <input type="text" name="email"  value="<?php echo $user[3] ?>" />
                </label>
                <label>Password
                    <input type="text" name="password"  value="<?php echo $user[4] ?>" />
                </label>
                <label>Privileges
                    <input type="text" name="role"  value="<?php echo $user[5] ?>" />
                </label>
                <div>
                    <h2>Select a User Group</h2>
                    <ul class="groups-check-list">
                        <?php foreach ($users->all_groups() as $g): ?>
                            <li>
                                <input
                                      name="groups[]"
                                      type="checkbox"
                                      value="<?php echo $g[0]; ?>"
                                      id="ch-<?php echo $g[0]; ?>"
                                      <?php
                                        if (in_array($g[0], $groups))
                                            echo 'checked=checked';
                                        ?>
                                    >
                                <label for="ch-<?php echo $g[0]; ?>"><?php echo $g[1] ?></label>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
                <input type="hidden" name="id" value="<?php echo $user[0] ?>" />
                <input type="hidden" name="user_action" value="edit" />
                <p>
                    <input type="submit" class="button right" />
                    <a href="index.php" class="button alert right">Cancel</a>
                </p>
            </form>
        </div>
    </div>

<?php require('footer.php'); ?>