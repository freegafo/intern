<?php
require('data.php');

$users = new Users();

$user_action = $_POST['user_action'];
$user['user'] = $_POST['username'];
$user['name'] = $_POST['name'];
$user['email'] = $_POST['email'];
$user['password'] = $_POST['password'];
$user['role'] = $_POST['role'];
$user['groups'] = $_POST['groups'];

if ('edit' == $user_action) {
    $user['id'] = $_POST['id'];
    echo $users->user_update($user);
}else {
    echo $users->user_add($user);
}