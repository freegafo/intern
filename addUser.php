<?php
    require('data.php');
    require('header.php');

    $users = new Users();
?>

    <div class="row">
        <div class="large-12 columns">
            <h1>Add User</h1>
            <form action="saveUser.php" method="post">
                <label>User
                    <input type="text" name="username"  />
                </label>
                <label>Name
                    <input type="text" name="name" />
                </label>
                <label>Email
                    <input type="text" name="email" />
                </label>
                <label>Password
                    <input type="text" name="password"  />
                </label>
                <label>Privileges
                    <input type="text" name="role" />
                </label>
                <div>
                    <h2>Select a User Group</h2>
                    <ul class="groups-check-list">
                        <?php foreach ($users->all_groups() as $g): ?>
                            <li>
                                <input name="groups[]" type="checkbox" value="<?php echo $g[0]; ?>" id="ch-<?php echo $g[0]; ?>">
                                <label for="ch-<?php echo $g[0]; ?>"><?php echo $g[1] ?></label>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
                <input type="hidden" name="user_action" value="add" />
                <p>
                    <input type="submit" class="button right" />
                    <a href="index.php" class="button alert right">Cancel</a>
                </p>
            </form>
        </div>
    </div>

<?php require('footer.php'); ?>