<?php
require('data.php');

$users = new Users();

$user_action = $_POST['user_action'];
$group['name'] = $_POST['name'];

if ('edit' == $user_action) {
    $group['id'] = $_POST['id'];
    echo $users->group_update($group);
}else {
    echo $users->group_add($group);
}