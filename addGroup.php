<?php
require('data.php');
require('header.php');

?>

    <div class="row">
        <div class="large-12 columns">
            <h1>Add Group</h1>

            <form action="saveGroup.php" method="post">
                <label>Neme
                    <input type="text" name="name"  />
                </label>

                <input type="hidden" name="user_action" value="add" />
                <p>
                    <input type="submit" class="button right" />
                    <a href="index.php" class="button alert right">Cancel</a>
                </p>
            </form>
        </div>
    </div>

<?php require('footer.php'); ?>